package squaro.cui;

/**
 * Exception thrown in case no console has been found
 */
public class ConsoleException extends RuntimeException{
	public ConsoleException(String message){
		super(message);
	}
	
	public ConsoleException(){
		super();
	}
}
