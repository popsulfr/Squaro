package squaro.cui;

import squaro.grid.*;

import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 * The console Interface
 * 
 * @author Philipp Richter
 */
public final class ConsoleInterface{
	
	private final Console _console;
	private Grid _grid;
	
	// This variable here is to avoid having subsequent grind printings
	private boolean _wasPrintGridJustInvoked;
	
	/**
	 * Construct a new ConsoleInterface object
	 * 
	 * @throws ConsoleException in case no console has been found
	 */
	public ConsoleInterface() throws ConsoleException{
		this._console=System.console();
		if(this._console==null) throw new ConsoleException("Unable to access a console!");
		this._grid=null;
		this._wasPrintGridJustInvoked=false;
	}
	
	/**
	 * Bring up the squaro Menu while loading a specific file
	 * 
	 * @param path the path to the file
	 */
	public void gameFromFileHuman(String path){
		try{
			this._grid=new Grid(this,new File(path));
		}
		catch(Exception e){
			printf("%s%n",e.getMessage());
		}
		gameMenu();
	}
	
	/**
	 * Let the engine find a solution to a squaro grid
	 * 
	 * @param path the path to the file
	 */
	public void gameFromFileComputer(String path){
		try{
			this._grid=new Grid(this,new File(path));
			findSolution();
		}
		catch(Exception e){
			printf("%s%n",e.getMessage());
		}
	}
	
	/**
	 * readline() method from Console
	 */	
	private String readLine(String fmt,Object... args){
		return this._console.readLine(fmt,args);
	}
	
	/**
	 * printf() from Console
	 */
	public void printf(String format,Object... args){
		this._console.printf(format,args);
	}
	
	/**
	 * Brings up the game Menu
	 */
	public void gameMenu(){
		boolean loop;
		do{
			loop=true;
		int index=0;
		if(this._grid!=null){
			if(!this._wasPrintGridJustInvoked) printGrid(this._grid);
			this._wasPrintGridJustInvoked=false;
		}
		printf("%d) Generate Random Grid with Solution%n",++index);
		printf("%d) Generate Random Grid with no Solution%n",++index);
		printf("%d) Load Solution...%n",++index);
		if(this._grid!=null){ printf("%d) Save Grid...%n",++index);
		printf("%d) Toggle a Bullet.%n",++index);
		printf("%d) Give Solution.%n",++index);
		printf("%d) Reset Grid.%n",++index);}
		printf("q) Quit%n");
		try{
			Matcher m=Pattern.compile("\\s*([1-"+index+"]|q)\\s*$").matcher(readLine("Input:"));
			m.find();
			switch(m.group(1).charAt(0)){
				case '1':
				randomGridCreate();
				break;
				case '2':
				randomNoSolutionGridCreate();
				break;
				case '3':
				loadFile();
				break;
				case '4':
				saveFile();
				break;
				case '5':
				toggleBullet();
				break;
				case '6':
				findSolution();
				break;
				case '7':
				if(this._grid!=null) this._grid.clearBulletStates();
				break;
				case 'q':
				loop=false;
				printf("Quit%n");
				break;
				default:
				printf("Unrecognized option!%n");}
		}
		catch(Exception e){printf("Unrecognized option!%n");}
	}while(loop);
	}
	
	/**
	 * Toggle a bullet state
	 */
	private void toggleBullet(){
		if(this._grid!=null){
		printf("Toggle Bullet%n");
		while(true){
			int row=-1;
			int column=-1;
			Random r=new Random();
			String input=readLine("Format l[1-%d]c[1-%d] (ex: l"+(r.nextInt(this._grid.getRows()+1)+1)+"c"+(r.nextInt(this._grid.getColumns()+1)+1)+"), q to quit, p to print grid: ",this._grid.getRows()+1,this._grid.getColumns()+1);
			try{ 
				Matcher m=Pattern.compile("\\s*(p|q)\\s*$").matcher(input);
				m.find();
				String opt=m.group(1);
				if(opt.charAt(0)=='p'){ printGrid(this._grid); continue;}
				else if(opt.charAt(0)=='q') { break; }
			}catch(IllegalStateException ise){}
			catch(IndexOutOfBoundsException iofbe){}
			catch(Exception e){}
			try{
				Matcher m=Pattern.compile("l\\s*(-?\\d+)\\s*c(-?\\d+)$").matcher(input);
				m.find();
				row=Integer.parseInt(m.group(1));
				column=Integer.parseInt(m.group(2));
				if(!this._grid.toggleBullet(row,column)) printf("Values are out of bounds!%n");
			}catch(IllegalStateException ise){
				printf("The String you supplied is abnormal!%n");}
			catch(IndexOutOfBoundsException iofbe){}
			catch(NumberFormatException nfe){
				printf("The numbers are abnormal! :(%n");}
			catch(Exception e){}
		}
	}
	}
	
	/**
	 * Try to find a solution
	 */	
	private void findSolution(){
		if(this._grid!=null){
			if(!this._grid.findSolution()) this._grid.findNoSolution();
		}
	}
	
	/**
	 * Generate a new grid with no real solution
	 */
	private void randomNoSolutionGridCreate(){
		printf("Random Grid No Solution%n");
		while(!gridCreate());
		if(this._grid!=null) this._grid.randomCells();
	}
	
	/**
	 * Save a squaro grid
	 */
	private void saveFile(){
		if(this._grid!=null){
		printf("Save grid%n");
		try{
		this._grid.fileSaver(new File(readLine("File Path: ")));
	}catch(Exception e){printf("%s%n",e.getMessage());}
	}
	}
	
	/**
	 * Load a squaro grid
	 */	
	private void loadFile(){
		printf("Load solution%n");
		try{
		this._grid=new Grid(this,new File(readLine("File Path: ")));
		printGrid(this._grid);
	}catch(Exception e){printf("%s%n",e.getMessage());}
	}
	
	/**
	 * Generate a random squaro grid with a solution
	 */			
	private void randomGridCreate(){
		printf("Random Grid Solution%n");
		while(!gridCreate());
		if(this._grid!=null) this._grid.randomGame();
	}
	
	/**
	 * Generate a blank grid (can not be worked with yet)
	 */
	private boolean gridCreate(){
		try{
			Pattern p=Pattern.compile("\\s*(-?\\d+)\\s*");
			Matcher mrow=p.matcher(readLine("Rows: "));
			mrow.find();
			int rows=Integer.parseInt(mrow.group(1));
			Matcher mcolumn=p.matcher(readLine("Columns: "));
			mcolumn.find();
			int columns=Integer.parseInt(mcolumn.group(1));
			this._grid=new Grid(this,rows,columns);
			return true;
		}catch(IllegalArgumentException iea){printf("%s%n",iea.getMessage());}
			catch(Exception e){printf("Unacceptable Value provided!%n");}
		return false;		
	}
		 
		
	/**
	 * Prints the grid in an esthetic fashion
	 * 
	 * @param grid the Grid object to print to console
	 * @param itrbullets an iterator over the bullets to be drawn
	 */
	public void printGrid(Grid grid,Iterator<Bullet> itrbullets){
		Iterator<Cell> itrcells=grid.getCellIterator();
		printf("v: %s%n*: %s%n^: %s%nx: enabled%no: disabled%n",BulletAmount.UNDERFLOW,BulletAmount.EXACT,BulletAmount.OVERFLOW);
		int rindex=1;
		int cindex=1;
		for(int r=0;r<grid.getRows()*2+3;r++){
			for(int c=0;c<grid.getColumns()*2+3;c++){
				if(r%2==0){
					if(c%2==0){
						if(r<2){
							if(c<2){
								for(int i=0;i<Integer.toString(grid.getRows()+1).length();i++) printf(" ");
							}
							else{
								printf("%d",cindex++);
							}
						}
						else{
						if(c<2){
							for(int i=0;i<Integer.toString(grid.getRows()+1).length()-Integer.toString(rindex).length();i++) printf(" ");
							printf("%d",rindex++);
						}
						else{
							if(itrbullets.next().getState()) printf("x");
							else printf("o");
						}
					}
					}
					else{
						if(r<2){
							if(c<2){
								printf("  ");
							}
							else{
								for(int i=0;i<4-Integer.toString(cindex-1).length();i++) printf(" ");
							}
						}
						else{
						if(c<2){
							printf("--");
						}
						else{
							printf("---");
						}
					}
					}
				}
				else{
					if(c%2==0){
						if(r<2){
							if(c<2){
								for(int i=0;i<Integer.toString(grid.getRows()+1).length();i++) printf(" ");
							}
							else{
								printf("|");
							}
						}
						else{
						if(c<2){
							for(int i=0;i<Integer.toString(grid.getRows()+1).length();i++) printf(" ");}
						else{
							printf("|");
						}
					}
					}
					else{
						if(r<2){
							if(c<2){
								printf("  ");
							}
							else{
								printf("   ");
							}
						}
						else{
						if(c<2){
							printf("  ");}
							else{
						Cell currCell=itrcells.next();
						switch(currCell.getBulletAmount()){
							case UNDERFLOW:
							printf("v%dv",currCell.getDigit());
							break;
							case EXACT:
							printf("*%d*",currCell.getDigit());
							break;
							case OVERFLOW:
							printf("^%d^",currCell.getDigit());
							break;
						}
					}
				}
					}
				}
			}
			printf("%n");
		}
		printf("Score: %d/%d%n",grid.violatedCells(),grid.getRows()*grid.getColumns());
		this._wasPrintGridJustInvoked=true;
	}
	
	/**
	 * Print the working grid
	 * 
	 * @param grid the grid to be printed
	 */
	public void printGrid(Grid grid){
		printGrid(grid,grid.getBulletIterator());
	}
	
	/**
	 * Print the solution grid
	 * 
	 * @param grid the grid to be printed
	 */
	public void printGridSolution(Grid grid){
		printGrid(grid,grid.getSolutionIterator());
	}
}
