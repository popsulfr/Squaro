#!/bin/sh
if [ -d bin ] ; then
	rm -r bin/*
else
	mkdir bin
fi
javac -sourcepath src -classpath bin -d bin src/squaro/Main.java
exit $?
