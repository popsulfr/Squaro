package squaro.grid;

/**
 * To represent the state of a Cell use this enum.
 * 
 * @author Philipp Richter
 */
public enum BulletAmount{
	/**
	 * The amount of Bullets activated is still not enough for the digit.
	 */
	UNDERFLOW,
	/**
	 * The amount of Bullets activated matches the digit.
	 */
	EXACT,
	/**
	 * The amount of Bullets activated is more than the digit.
	 */
	OVERFLOW};
