#!/bin/sh
if [ -d doc ] ; then
	rm -r doc/*
else
	mkdir doc
fi
javadoc -sourcepath src -classpath bin -d doc src/squaro/{*,*/*}.java
exit $?
