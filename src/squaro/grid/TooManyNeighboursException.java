package squaro.grid;

/**
 * This Exception should be thrown in case too many neighbours are offered to a given object.
 * 
 * @author Philipp Richter
 */
public class TooManyNeighboursException extends IndexOutOfBoundsException{
	
	public TooManyNeighboursException(){
		super();
	}
	
	public TooManyNeighboursException(String message){
		super(message);
	}
}
