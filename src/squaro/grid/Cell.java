package squaro.grid;

/**
 * In our game the Cell object stands for a container holding a numeric value.
 * It stores access to its neighbouring Bullets.
 * 
 * @author Philipp Richter
 */
public class Cell implements Cloneable{
	private static int ID=-1;
	
	/**
	 * The amount of neighbours surrounding the cell.
	 * The system is not flexible enough to allow for change without breaking stuff.
	 */
	public static final int MAX_NEIGHBOURS=4;
	
	private int _uniqueID;
	private int _digit;
	private BulletAmount _bulletAmount;
	private Bullet[] _neighbours;
	private int _size;
	private int _enabledBullets;
	
	/**
	 * Constructs a new Cell object.
	 * 
	 * A unique ID is generated for each created Cell.
	 * {@link #hashCode()} to fetch the ID.
	 * Use {@link #resetID()} to reset the generator.
	 * 
	 * @param digit the digit that will be stored in the Cell.
	 * @throws IllegalArgumentException if supplied with an unacceptable digit.
	 */
	public Cell(int digit){
		setDigit(digit);
		setBulletAmount(0);
		this._uniqueID=++ID;
		this._neighbours=new Bullet[MAX_NEIGHBOURS];
		this._size=0;
		this._enabledBullets=0;
	}
	
	/**
	 * Get the Bullet Amount
	 * 
	 * @see BulletAmount
	 * 
	 * @return the bulletamount repercussion on the Cell.
	 */
	public BulletAmount getBulletAmount(){
		return this._bulletAmount;
	}
	
	/**
	 * Sell a new bullet amount
	 * 
	 * @param ba new bullet amount
	 */
	private void setBulletAmount(int ba){
		this._enabledBullets=ba;
		if(ba<this._digit){
			this._bulletAmount=BulletAmount.UNDERFLOW;}
		else if(ba==this._digit){
			this._bulletAmount=BulletAmount.EXACT;}
		else{
			this._bulletAmount=BulletAmount.OVERFLOW;}
	}
	
	/**
	 * This function scans its neighbouring Bullets and changes its state accordingly.
	 */
	public void checkState(){
		int ba=0;
		for(int i=0;i<this._size;i++){
			if(this._neighbours[i].getState()){ ++ba;}
		}
		setBulletAmount(ba);
	}
	
	/**
	 * Get the amount of enabled bullets surrounding the cell
	 * 
	 * @return the amount of enabled bullets surrounding the cell
	 */
	public int getEnabledBullets(){
		return this._enabledBullets;
	}
	
	/**
	 * Resets the surrounding bullets to false.
	 */
	public void clearBulletState(){
		for(int i=0;i<this._size;i++){
			this._neighbours[i].setState(false);
		}
	}
	
	/**
	 * Get the digit
	 * 
	 * @return the digit of the Cell.
	 */
	public int getDigit(){
		return this._digit;
	}
	
	/**
	 * Set digit to a new value
	 * 
	 * @param digit the new digit that shall replace the former one.
	 * @throws IllegalArgumentException if supplied with an unacceptable digit.
	 */
	public void setDigit(int digit){
		if(digit<0 || digit>MAX_NEIGHBOURS) throw new IllegalArgumentException("Supplied Value is out of the acceptable Range: "+digit+">"+MAX_NEIGHBOURS);
		this._digit=digit;
	}
	
	/**
	 * Add a new neighbour
	 * 
	 * @param b a Bullet you wish to add to the Cell's neighbours
	 * @throws TooManyNeighboursException if all seats are already taken
	 */
	public void addNeighbour(Bullet b) throws TooManyNeighboursException{
		if(this._size>=MAX_NEIGHBOURS) throw new TooManyNeighboursException("More neighbours than available places.");
		this._neighbours[this._size++]=b;
	}
	
	/**
	 * Get neighbour number index
	 * 
	 * @param index the index of a neighbour
	 * @return the demanded neighbour
	 */
	public Bullet getNeighbour(int index){
		return this._neighbours[index];
	}
	
	/**
	 * Clears the surrounding neighbours
	 */
	public void clearNeighbours(){
		this._size=0;
		this._neighbours=new Bullet[MAX_NEIGHBOURS];
	}
	
	/**
	 * Get the amount of neighbours
	 * 
	 * @return the number of neighbours
	 */
	public int getSize(){
		return this._size;
	}
	
	/**
	 * Resets the unique ID to start from scratch.
	 */
	public static void resetID(){
		ID=-1;
	}
	
	/**
	 * Overwritten hashCode method return the unique ID
	 * @return unique Cell ID
	 */
	public int hashCode(){
		return this._uniqueID;
	}
	
	public boolean equals(Cell c){
		return this.hashCode()==c.hashCode();
	}
	
	/**
	 * Clones a cell object keeping the bullet neighbours' references
	 *
	 * @return the cloned cell
	 */
	public Cell clone(){
		Cell newCell=new Cell(getDigit());
		this.ID--;
		newCell._uniqueID=this._uniqueID;
		for(int i=0;i<this._size;i++){ newCell._neighbours[i]=this._neighbours[i];}
		newCell._size=this._size;
		return newCell;
	}
	
	/**
	 * Clones a cell object with dummy bullet neighbours
	 * 
	 * @return the cloned cell
	 */
	public Cell cloneDummy(){
		Cell newCell=new Cell(getDigit());
		this.ID--;
		newCell._uniqueID=this._uniqueID;
		for(int i=0;i<this._size;i++){
			Bullet b=new Bullet(false);
			b.addNeighbour(newCell);
			newCell.addNeighbour(b);
		}
		return newCell;
	}
}
