package squaro.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * A little Grid generation dialog
 * 
 * @author Philipp Richter
 */
class GuiGridCreator extends JDialog{
	
	private Gui _parent;
	//Gui components declarations
	private JSpinner _rowSpinner;
	private JSpinner _columnSpinner;
	private JCheckBox _choiceSolution;
	private JButton _generateButton;
	
	private void init(){
		//Defaults
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setResizable(false);
		//Gui components init
		this._rowSpinner=new JSpinner(new SpinnerNumberModel(3,1,80,1));
		this._columnSpinner=new JSpinner(new SpinnerNumberModel(3,1,80,1));
		this._choiceSolution=new JCheckBox("Must have a solution",true);
		this._generateButton=new JButton("Generate");
		JPanel rowPanel=new JPanel();
		JPanel columnPanel=new JPanel();
		//Gui layouts init
		getContentPane().setLayout(new BoxLayout(getContentPane(),BoxLayout.PAGE_AXIS));
		//Gui components tweaks
		this._rowSpinner.setAlignmentX(Component.CENTER_ALIGNMENT);
		this._columnSpinner.setAlignmentX(Component.CENTER_ALIGNMENT);
		rowPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		columnPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this._choiceSolution.setAlignmentX(Component.CENTER_ALIGNMENT);
		this._generateButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		// ActionListeners
		this._generateButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				generate();}});
		//Gui components insertion
		rowPanel.add(new JLabel("Rows"));
		rowPanel.add(this._rowSpinner);
		getContentPane().add(rowPanel);
		columnPanel.add(new JLabel("Columns"));
		columnPanel.add(this._columnSpinner);
		getContentPane().add(columnPanel);
		getContentPane().add(this._choiceSolution);
		getContentPane().add(this._generateButton);
		// Finally
		pack();
	}
	
	/**
	 * Call the parent window to generate a grid according to the informations gathered
	 */
	private void generate(){
		if(this._choiceSolution.isSelected()){
			this._parent.generateRandomGameWithSolution(((Integer)(this._rowSpinner.getValue())).intValue(),((Integer)(this._columnSpinner.getValue())).intValue());
		}
		else{
			this._parent.generateRandomGameNoSolution(((Integer)(this._rowSpinner.getValue())).intValue(),((Integer)(this._columnSpinner.getValue())).intValue());
		}
		setVisible(false);
	}
		
	/**
	 * Construct a GuiGridCreator object
	 * 
	 * @param parent the main frame
	 */
	GuiGridCreator(Gui parent){
		super(parent,"Grid Creation");
		this._parent=parent;
		init();
	}
}
