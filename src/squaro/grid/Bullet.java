package squaro.grid;

/** 
 * The Bullet is the toggable object placed on the cells' intersections
 * Stores the neighbouring Cells.
 * 
 * @author Philipp Richter
 */
public class Bullet implements Cloneable{
	private static int ID=-1;
	
	/**
	 * The amount of neighbours surrounding the Bullet.
	 * The system is not flexible enough to allow for change without breaking stuff.
	 */
	public static final int MAX_NEIGHBOURS=4;
	
	private int _uniqueID;
	private boolean _state;
	private Cell[] _neighbours;
	private int _size;
	
	/**
	 * Constructs a new Bullet Object
	 * 
	 * A unique ID is generated for each created Cell.
	 * {@link #hashCode()} to fetch the ID.
	 * Use {@link #resetID()} to reset the generator.
	 */
	public Bullet(boolean state){
		this._state=state;
		this._uniqueID=++ID;
		this._neighbours=new Cell[MAX_NEIGHBOURS];
		this._size=0;
	}
	
	/**
	 * Constructs a new Bullet Object with state uninitiated
	 * 
	 * A unique ID is generated for each created Cell.
	 * {@link #hashCode()} to fetch the ID.
	 * Use {@link #resetID()} to reset the generator.
	 */
	public Bullet(){
		this(false);
	}
	
	/**
	 * Get the state of the Bullet.
	 * 
	 * @return the state of the Bullet
	 */
	public boolean getState(){
		return this._state;
	}
	
	/**
	 * Changing the state to its current opposite will summon a check on all of its neighbouring cells
	 * Cellstates will be altered accordingly.
	 * 
	 * @param state the new state of the Bullet
	 */
	public void setState(boolean state){
		if(state!=this._state){
			this._state=state;
			for(int i=0;i<this._size;i++){
				this._neighbours[i].checkState();
			}
		}
	}
	
	/**
	 * Check if one or more Cells have a Bullet Overflow
	 * 
	 * @return true if one or more cells have a Bullet Overflow
	 */
	public boolean hasOverflowingNeighbours(){
		boolean thetruth=false;
		for(int i=0;i<this._size;i++){
			if(this._neighbours[i].getBulletAmount()==BulletAmount.OVERFLOW){
				thetruth=true;
				break;
			}
		}
		return thetruth;
	}
	
	/**
	 * Add a neighbour.
	 * 
	 * @param c a Cell to be added the the Bullet's neighbours
	 * @throws TooManyNeighboursException when all seats are already taken
	 */
	public void addNeighbour(Cell c){
		if(this._size>=MAX_NEIGHBOURS) throw new TooManyNeighboursException();
		this._neighbours[this._size++]=c;
	}
	
	/**
	 * Get a Neighbour at index.
	 * 
	 * @param index the index of the neighbour
	 */
	public Cell getNeighbour(int index){
		return this._neighbours[index];
	}
	
	/**
	 * Clears all neighbours.
	 */
	public void clearNeighbours(){
		this._size=0;
		this._neighbours=new Cell[MAX_NEIGHBOURS];
	}
	
	/**
	 * Give the current amount of neighbours.
	 * 
	 * @return amount of neighbours
	 */
	public int getSize(){
		return this._size;
	}
	
	/**
	 * Reset the unique ID of the Bullet.
	 */
	public static void resetID(){
		ID=-1;
	}
	
	/**
	 * Gives the unique ID of the bullet.
	 * 
	 * @return the unique ID of the Bullet
	 */
	public int hashCode(){
		return this._uniqueID;
	}
	
	public boolean equals(Bullet b){
		return this.hashCode()==b.hashCode();
	}
	
	/**
	 * Clones a bullet keeping the neighbours' references
	 * 
	 * @return the cloned bullet
	 */
	public Bullet clone(){
		Bullet newBullet=new Bullet(this._state);
		this.ID--;
		newBullet._uniqueID=this._uniqueID;
		for(int i=0;i<this._size;i++){ newBullet._neighbours[i]=this._neighbours[i];}
		newBullet._size=this._size;
		return newBullet;
	}
}
