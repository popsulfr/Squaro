package squaro.gui;

import squaro.Main;
import squaro.grid.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

/**
 * This class represents the graphical user interface
 * 
 * @author Philipp Richter
 */
public final class Gui extends JFrame{
	
	private final static String RESOURCE_FOLDER="res/";
	private String _windowTitle="SquarO";
	
	private Grid _funcGrid;
	private HashMap<JToggleButton,Bullet> _toggleButtonsMap;
	private HashMap<JLabel,Cell> _cellMap;
	private int _score;
	private Thread _solutionThread;
	
	// Image icons
	private String _bulletPath="bullet.png";
	private String _bullethighPath="bullet_high.png";
	private String _linehorPath="line_hor.png";
	private String _linevertPath="line_vert.png";
	private ImageIcon _bullet;
	private ImageIcon _bullethigh;
	private ImageIcon _linehor;
	private ImageIcon _linevert;
	
	// Gui components
	// Gui Components declarations
	private JMenuBar _menuBar;
	private JMenu _fileMenu;
	private JMenuItem _gridCreatorMenuItem;
	private JMenuItem _solutionMenuItem;
	private JMenuItem _loadFileMenuItem;
	private JMenuItem _saveFileMenuItem;
	private JMenuItem _closeMenuItem;
	private JPanel _mainPanel;
	private GuiGridCreator _ggc;
	private JOptionPane _abortSolutionPane;
	private JDialog _abortSolutionDialog;
	private javax.swing.Timer _abortSolutionTimer;
	
	private javax.swing.Timer _gameTimer;
	private JPanel _topPanel;
	private JPanel _bottomPanel;
	private JLabel _scoreLabel;
	private JLabel _timerLabel;
	private JButton _resetButton;
	private int _clockedTime;
	
	//Font
	private Font _font;
	
	// Gui layouts
	private GridBagLayout _mainPanelGridBag;
	private GridBagConstraints _mainPanelGridBagConstraints;
	
	// Image initialisation
	private void imageInit(){
		this._bullet=new ImageIcon(this.RESOURCE_FOLDER+this._bulletPath);
		this._bullethigh=new ImageIcon(this.RESOURCE_FOLDER+this._bullethighPath);
		this._linehor=new ImageIcon(this.RESOURCE_FOLDER+this._linehorPath);
		this._linevert=new ImageIcon(this.RESOURCE_FOLDER+this._linevertPath);
	}		
		
	// Gui components initialisation
	private void init(){
		this._score=0;
		this._toggleButtonsMap=new HashMap<JToggleButton,Bullet>();
		this._cellMap=new HashMap<JLabel,Cell>();
		this._clockedTime=0;
		// Defaults
		setTitle(this._windowTitle);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setLocationRelativeTo(null);
		// Gui components init
		this._menuBar=new JMenuBar();
		this._fileMenu=new JMenu("File");
		this._gridCreatorMenuItem=new JMenuItem("Generate a Grid");
		this._solutionMenuItem=new JMenuItem("Give Solution");
		this._loadFileMenuItem=new JMenuItem("Load a Grid...");
		this._saveFileMenuItem=new JMenuItem("Save a Grid...");
		this._closeMenuItem=new JMenuItem("Close");
		this._mainPanel=new JPanel();
		this._topPanel=new JPanel();
		this._bottomPanel=new JPanel();
		this._scoreLabel=new JLabel();
		this._timerLabel=new JLabel();
		setTimerLabel();
		this._resetButton=new JButton("Reset");
		this._gameTimer=new javax.swing.Timer(1000,new ActionListener(){
			public void actionPerformed(ActionEvent e){
				timerIncrease();}});
		this._ggc=new GuiGridCreator(this);
		this._font=new Font(Font.SANS_SERIF,Font.PLAIN,20);
		this._abortSolutionPane=new JOptionPane("The application is taking a long time to find a Solution.\nDo you wish to abort the process?",JOptionPane.QUESTION_MESSAGE,JOptionPane.YES_NO_OPTION);
		this._abortSolutionTimer=new javax.swing.Timer(6000,new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(_solutionThread.isAlive()) abortSolutionFinderDialog();}});
		// Gui layouts init
		this._mainPanelGridBag=new GridBagLayout();
		this._mainPanelGridBagConstraints=new GridBagConstraints();
		// Gui components tweaking
		setScoreLabel();
		this._abortSolutionTimer.setRepeats(false);
		this._gameTimer.setRepeats(true);
		this._scoreLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this._scoreLabel.setFont(this._font);
		this._timerLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this._timerLabel.setFont(this._font);
		this._resetButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		getContentPane().setLayout(new BorderLayout());
		this._topPanel.setLayout(new BoxLayout(this._topPanel,BoxLayout.PAGE_AXIS));
		//this._bottomPanel.setLayout(new BoxLayout(this._bottomPanel,BoxLayout.PAGE_AXIS));
		this._mainPanel.setLayout(this._mainPanelGridBag);
		this._mainPanel.setPreferredSize(new Dimension(300,300));
		this._mainPanel.setMinimumSize(new Dimension(300,300)); // Bug: Minimum size is not enforced!
		// ActionListeners
		this._gridCreatorMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				_ggc.setVisible(true);
			}});
		this._solutionMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				giveSolution();}});
		this._loadFileMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				loadFile();}});
		this._saveFileMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				saveFile();}});
		this._closeMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();}});
		this._resetButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				cleanGrid();}});
		// Gui components insertion
		setJMenuBar(this._menuBar);
		this._menuBar.add(this._fileMenu);
		this._fileMenu.add(this._gridCreatorMenuItem);
		this._fileMenu.add(this._solutionMenuItem);
		this._fileMenu.add(this._loadFileMenuItem);
		this._fileMenu.add(this._saveFileMenuItem);
		this._fileMenu.add(this._closeMenuItem);
		this._topPanel.add(this._scoreLabel);
		this._topPanel.add(this._timerLabel);
		this._bottomPanel.add(this._resetButton);
		getContentPane().add(this._mainPanel,BorderLayout.CENTER);
		getContentPane().add(this._topPanel,BorderLayout.NORTH);
		getContentPane().add(this._bottomPanel,BorderLayout.SOUTH);
		// Finally
		pack();
		centerWindow(this);
		centerWindow(this._ggc);
		setVisible(true);
	}
	
	/**
	 * Center a window on the screen
	 * 
	 * @param w the window you wish center on the screen
	 */
	static void centerWindow(Window w){
		Dimension screend=Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowd=w.getSize();
		w.setLocation((int)((screend.getWidth()/2)-(windowd.getWidth()/2)),(int)((screend.getHeight()/2)-(windowd.getHeight()/2)));
	}
	
	/**
	 * Generate a graphical grid	
	 */
	private void createGrid(){
		this._funcGrid.abortSolutionFinder();
		stopTimer();
		this._mainPanel.removeAll();
		this._cellMap.clear();
		this._toggleButtonsMap.clear();
		Iterator<Bullet> itrbullet=this._funcGrid.getBulletIterator();
		Iterator<Cell> itrcell=this._funcGrid.getCellIterator();
		for(int i=0;i<(this._funcGrid.getRows()*2)+1;i++){
			for(int j=0;j<(this._funcGrid.getColumns()*2)+1;j++){
				this._mainPanelGridBagConstraints.gridx=j;
				this._mainPanelGridBagConstraints.gridy=i;
				if(i%2==0){
					if(j%2==0){
						Bullet b=itrbullet.next();
						JToggleButton button=new JToggleButton(this._bullet);
						button.setSelectedIcon(this._bullethigh);
						button.setPressedIcon(this._bullethigh);
						button.setRolloverIcon(this._bullethigh);
						button.setPreferredSize(new Dimension(this._bullethigh.getIconWidth(),this._bullethigh.getIconHeight()));
						button.setBorderPainted(false);
						button.setContentAreaFilled(false);
						button.setSelected(b.getState());
						button.addActionListener(new ActionListener(){
							public void actionPerformed(ActionEvent e){
								toggleButtonAction(e);}});
						this._toggleButtonsMap.put(button,b);
						this._mainPanel.add(button,this._mainPanelGridBagConstraints);}
					else{
						this._mainPanel.add(new JLabel(this._linehor),this._mainPanelGridBagConstraints);}}
				else{
					if(j%2==0){
						this._mainPanel.add(new JLabel(this._linevert),this._mainPanelGridBagConstraints);}
					else{
						Cell c=itrcell.next();
						JLabel labelCell=new JLabel(""+c.getDigit());
						labelCell.setFont(this._font);
						setCellTextColor(labelCell,c.getBulletAmount());
						this._cellMap.put(labelCell,c);
						this._mainPanel.add(labelCell,this._mainPanelGridBagConstraints);}}
			}
		}
		this._mainPanel.setPreferredSize(null);
		this._mainPanel.revalidate();
		this._mainPanel.repaint();
		this._score=this._funcGrid.violatedCells();
		setScoreLabel();
		startTimer();
		pack();
	}
	
	private void setTimerLabel(){
		this._timerLabel.setText("Timer: "+(this._clockedTime/60)+"m"+((Integer.toString(60).length()-Integer.toString(this._clockedTime%60).length())==1?"0"+this._clockedTime%60:this._clockedTime%60)+"s");
	}
	
	private void setScoreLabel(){
		if(this._funcGrid!=null) this._scoreLabel.setText("Score: "+this._score+"/"+(this._funcGrid.getRows()*this._funcGrid.getColumns()));
		else this._scoreLabel.setText("Score: "+this._score+"/"+0);
	}
	
	private void stopTimer(){
		this._gameTimer.stop();
	}
	
	private void startTimer(){
		this._gameTimer.stop();
		this._clockedTime=0;
		this._gameTimer.start();
	}
	
	/**
	 * A new random grid with solution is generated with the specified size
	 * 
	 * @param rows the amount of rows
	 * @param columns the amount of columns
	 */
	void generateRandomGameWithSolution(int rows,int columns){
		this._funcGrid=new Grid(Main.CI,rows,columns);
		this._funcGrid.randomGame();
		createGrid();
	}
	
	/**
	 * A new random grid with no solution is generated with the specified size
	 * 
	 * @param rows the amount of rows
	 * @param columns the amount of columns
	 */
	void generateRandomGameNoSolution(int rows,int columns){
		this._funcGrid=new Grid(Main.CI,rows,columns);
		this._funcGrid.randomCells();
		createGrid();
	}
	
	private void setCellTextColor(JLabel label, BulletAmount ba){
		switch(ba){
			case EXACT:
			label.setForeground(Color.green);
			break;
			case OVERFLOW:
			label.setForeground(Color.red);
			break;
			default:
			label.setForeground(Color.black);
		}
	}
	
	private void timerIncrease(){
		if(this._score==0){
			stopTimer();
		}
		else{
			this._clockedTime++;
			setTimerLabel();
		}
	}
	
	/**
	 * Resets the grid and interface
	 */	
	private void cleanGrid(){
		if(this._funcGrid!=null){
			this._funcGrid.abortSolutionFinder();
			stopTimer();
			this._funcGrid.clearBulletStates();
			this._score=this._funcGrid.violatedCells();
			setScoreLabel();
			startTimer();
			refreshGrid();
		}
	}
	
	/**
	 * Refreshes the grid with potentially updated values
	 */
	private void refreshGrid(){
		Iterator<Map.Entry<JLabel,Cell>> cellitr=this._cellMap.entrySet().iterator();
		Iterator<Map.Entry<JToggleButton,Bullet>> bulletitr=this._toggleButtonsMap.entrySet().iterator();
		while(cellitr.hasNext()){
			Map.Entry<JLabel,Cell> cmap=cellitr.next();
			setCellTextColor(cmap.getKey(),cmap.getValue().getBulletAmount());
		}
		while(bulletitr.hasNext()){
			Map.Entry<JToggleButton,Bullet> bmap=bulletitr.next();
			bmap.getKey().setSelected(bmap.getValue().getState());
		}
	}
		
	private void toggleButtonAction(ActionEvent e){
		JToggleButton selectedbutton=(JToggleButton)(e.getSource());
		this._toggleButtonsMap.get(selectedbutton).setState(selectedbutton.isSelected());
		this._score=this._funcGrid.violatedCells();
		setScoreLabel();
		refreshGrid();
		if(this._score==0) JOptionPane.showMessageDialog(this,"YOU WIN!","The Best of the Best...",JOptionPane.INFORMATION_MESSAGE);
	}
	
	private void abortSolutionFinderDialog(){
		this._abortSolutionDialog=this._abortSolutionPane.createDialog(this,"Time is money...");
		this._abortSolutionDialog.setVisible(true);
		if(_abortSolutionPane.getValue().equals(JOptionPane.YES_OPTION)){
			 this._funcGrid.abortSolutionFinder();}
		else if(this._solutionThread.isAlive()) this._abortSolutionTimer.start();
		this._abortSolutionDialog.setVisible(false);
	}
	
	/**
	 * Tell the window to look for a solution for the currently loaded grid
	 */
	public void giveSolution(){
		if(this._funcGrid!=null){
			setEnabled(false);
			this._solutionThread=new Thread(new Runnable(){
				public void run(){
					if(!_funcGrid.findSolution()) _funcGrid.findNoSolution();
					if(_abortSolutionDialog!=null) _abortSolutionDialog.setVisible(false);
					_abortSolutionTimer.stop();
					_score=_funcGrid.violatedCells();
					setScoreLabel();
					stopTimer();
					refreshGrid();
					setEnabled(true);}});
			this._abortSolutionTimer.start();
			this._solutionThread.start();
		}
	}
	
	/**
	 * Easy error popup window creator
	 * 
	 * @param message the message you wish to display in your popup
	 */
	private void errorPopup(String message){
		JOptionPane.showMessageDialog(this,message,"Shit happened...",JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Invoke save file open a dialog
	 */
	private void saveFile(){
		if(this._funcGrid!=null){
		try{
		JFileChooser fileChooser=new JFileChooser();
		fileChooser.setDialogTitle("Save a SquarO Grid...");
		if(fileChooser.showSaveDialog(this)==JFileChooser.APPROVE_OPTION){
			this._funcGrid.fileSaver(fileChooser.getSelectedFile());
		}
		}catch(Exception e){
			errorPopup(e.getMessage());
		}
		}
	}
	
	/**
	 * Invoke load file open a dialog
	 */	
	private void loadFile(){
		try{
		JFileChooser fileChooser=new JFileChooser();
		fileChooser.setDialogTitle("Load a SquarO Grid...");
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		if(fileChooser.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
			this._funcGrid=new Grid(Main.CI,fileChooser.getSelectedFile());
			createGrid();
		}
	}catch(Exception e){
		errorPopup(e.getMessage());
	}
	}
	
	/**
	 * Constructs a new Window object
	 */	
	public Gui(){
		imageInit();
		init();
	}
	
	/**
	 * Constructs a new Window object with a Gridfile
	 * 
	 * @param path The path to the file to be loaded
	 */
	public Gui(String path){
		this();
		try{
			this._funcGrid=new Grid(Main.CI,new File(path));
			createGrid();
		}
		catch(Exception e){
			errorPopup(e.getMessage());
		}
	}
}
