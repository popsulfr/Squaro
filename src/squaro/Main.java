package squaro;

import squaro.cui.ConsoleInterface;
import squaro.gui.Gui;

import java.io.*;
/**
 * The main executable class
 * 
 * @author Philipp Richter
 */
public final class Main{
	
	/**
	 * Some classes like the Grid will need access to the ConsoleInterface Object to print stuff
	 */
	public static ConsoleInterface CI=null;
	
	// you may change the defaults here and it will be reflected in the application
	private static final boolean humanDef=true;
	private static final boolean computerDef=!humanDef;
	private static final boolean graphDef=true;
	private static final boolean textDef=!graphDef;
	
	// we don't want anyone creating an object from this class, would be quite useless
	private Main(){}
	
	/**
	 * The lovely main
	 */
	public static void main(String[] args){
		try{
			CI=new ConsoleInterface();
		}
		//catch(ConsoleException ce){}
		catch(Exception e){}
		parser(args);
	}
	
	/**
	 * A nice parsing function
	 */
	private static void parser(String[] args){
		String file=null;
		boolean human=false;
		boolean computer=false;
		boolean graph=false;
		boolean text=false;
		boolean help=false;
		boolean nextIsFile=false;
		for(String s:args){
			if(s!=null){
			if(!nextIsFile && s.equals("-")) nextIsFile=true;
			else if(!nextIsFile && (s.equals("--human") || s.equals("-human"))) human=true;
			else if(!nextIsFile && (s.equals("--computer") || s.equals("-computer"))) computer=true;
			else if(!nextIsFile && (s.equals("--graph") || s.equals("-graph"))) graph=true;
			else if(!nextIsFile && (s.equals("--text") || s.equals("-text"))) text=true;
			else if(!nextIsFile && (s.equals("--help") || s.equals("-help"))) help=true;
			else{
				nextIsFile=false;
				if(file!=null){
					if(CI!=null){
					CI.printf("You may not specify more than one file!%n%n");
					help();
				}
					return;
				}
				else{
					file=s;
				}
			}
		}
		}
		if(help || nextIsFile){
			help();
			return;
		}
		if(human && computer){
			if(CI!=null){
			CI.printf("You may only choose one of --human or --computer.%n%n");
			help();
		}
			return;
		}
		if(graph && text){
			if(CI!=null){
			CI.printf("You may only choose one of --graph or --text.%n%n");
			help();
		}
			return;
		}
		if(graph || ((!graph && graphDef) && !text)){
			if(computer || ((!computer && computerDef) && !human)){
				if(file!=null){
					Gui g=new Gui(file);
					g.giveSolution();
				}
				else CI.printf("You didn't provide me a file!%n");
			}
			else if(file!=null){
				new Gui(file);
			}
			else new Gui();
			return;
		}
		if(text || ((!text && textDef) && !graph)){
			if(CI!=null){
				if(computer || ((!computer && computerDef) && !human)){
					CI.printf("Computer Mode%n%n");
					if(file!=null) CI.gameFromFileComputer(file);
					else CI.printf("You didn't provide me a file!%n");
				}
				else if(human || ((!human && humanDef) && !computer)){
					CI.printf("Human Mode%n%n");
					if(file!=null) CI.gameFromFileHuman(file);
					else CI.gameMenu();
				}
				else{
					CI.printf("Human Mode%n%n");
					if(file!=null) CI.gameFromFileHuman(file);
					else CI.gameMenu();
				}
			}
			return;
		}
	}
	
	/**
	 * The help message
	 */
	private static void help(){
		if(CI!=null){
			CI.printf("Invocation:%nexecutable [--help] [--graph|--text] [--human|--computer FILE] [FILE]%n%n");
			CI.printf("  --help%n        Displays this help.%n%n");
			CI.printf("  FILE%n        A file you wish to load as a Grid to be solved.%n        Put a '-' before your file if you wish to load a file that starts with '-'.%n%n");
			CI.printf("  --graph"+(graphDef?" (default)":"")+"%n        Starts the game in graphical mode.%n");
			CI.printf("      --human"+(humanDef?" (default)":"")+"%n        The Resolution of the Grid is performed by a human.%n");
			CI.printf("      --computer"+(computerDef?" (default)":"")+"%n        The Resolution of the Grid is performed by the CPU.%n%n");
			CI.printf("  --text"+(textDef?" (default)":"")+"%n        Starts the game in console mode.%n");
			CI.printf("      --human"+(humanDef?" (default)":"")+"%n        The Resolution of the Grid is performed by a human.%n");
			CI.printf("      --computer"+(computerDef?" (default)":"")+"%n        The Resolution of the Grid is performed by the CPU.%n%n");
		}
	}
}
