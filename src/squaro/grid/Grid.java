package squaro.grid;

import squaro.Main;
import squaro.cui.ConsoleInterface;

import java.util.*;
import java.io.*;

/**
 * This class represents the grid of the game.
 * It stores the different Cells and Bullets.
 * 
 * @author Philipp Richter
 */
public class Grid{
	private List<Bullet> _bullets;
	private List<Cell> _cells;
	private List<Bullet> _bestSolution;
	
	private int _rows;
	private int _columns;
	
	private boolean _abortSolution;
	
	private ConsoleInterface _ci;
	
	/**
	 * Constructs a new Grid object of specified size
	 * 
	 * @param ci the ConsoleInterface
	 * @param rows the amount of cell rows
	 * @param columns the amount of cell columns
	 */
	public Grid(ConsoleInterface ci,int rows,int columns){
		this._ci=ci;
		this._abortSolution=false;
		gridInit(rows,columns);
	}
	
	/**
	 * Constructs a new Grid object from file
	 * 
	 * @param ci the ConsoleInterface
	 * @param file the file to load
	 * @throws FileNotFoundException if the file was not found
	 * @throws Exception for any other issue (See getMessage())
	 */
	public Grid(ConsoleInterface ci,File file) throws FileNotFoundException,Exception{
		this._ci=ci;
		this._abortSolution=false;
		fileLoader(file);
	}
	
	/**
	 * Loads a file into a grid object
	 * 
	 * @param file the file input stream
	 * @throws FileNotFoundException if the file was not found
	 * @throws Exception for any other stuff happens
	 */
	private void fileLoader(File file) throws FileNotFoundException,Exception{
		Scanner lineScanner=new Scanner(new BufferedInputStream(new FileInputStream(file)),"UTF-8");
		List<Integer> parsedCells=new ArrayList<Integer>();
		int rows=-1;
		int columns=-1;
		try{
		while(lineScanner.hasNextLine()){
			String currLine=lineScanner.nextLine();
			if(!currLine.isEmpty()){
				Scanner columnScanner=new Scanner(currLine);
				try{
				while(columnScanner.hasNextInt()){
					int currInt=columnScanner.nextInt();
					if(rows<0 || columns<0){
					if(rows<0){
						if(currInt<0){
							rows=0;
						}
						else{
							rows=currInt;
						}
					}
					else if(columns<0){
						if(currInt<0){
							columns=0;
						}
						else{
							columns=currInt;
						}
					}
				}
				else{
					parsedCells.add(new Integer(currInt));
				}
				}
			}
			catch(Exception e){throw e;}
			finally{
				columnScanner.close();}
			}
		}}
		catch(Exception e){ throw e; }
		finally{
		lineScanner.close();
	}
		if(rows*columns==parsedCells.size()){
			gridInit(rows,columns);
			Iterator<Integer> itrints=parsedCells.iterator();
			Iterator<Cell> itrcells=this._cells.iterator();
			while(itrints.hasNext()){
				Cell c=itrcells.next();
				c.setDigit(itrints.next());
				c.checkState();
			}
		}
		else{
			throw new InputMismatchException("Given size of the Grid does not match the actual content!");
		}
	}
	
	/**
	 * Saves the current grid into a file
	 * 
	 * @param file the file output stream to where to write
	 * @throws IOException if anything wrong happens while writing the file
	 */
	public void fileSaver(File file) throws IOException{
		BufferedWriter writer=new BufferedWriter(new FileWriter(file));
		try{
		writer.write(getRows()+" "+getColumns());
		writer.newLine();
		int limit=0;
		for(Cell c:this._cells){
			if(limit==0){
				writer.write(""+c.getDigit());
			}
			else{
				writer.write(" "+c.getDigit());
			}
			if(limit==getColumns()-1){
				writer.newLine();
				limit=0;
			}
			else{
				limit++;
			}
		}
	}
	catch(IOException e){ throw e;}
	finally{
		writer.close();
	}
}
			
	/**
	 * Initialisation of the grid with specified size
	 * 
	 * @param rows the amount of rows
	 * @param columns amount of columns
	 * @throws IllegalArgumentException if supplied values are not in acceptable rang
	 */
	private void gridInit(int rows,int columns){
		if(rows<1 || columns<1) throw new IllegalArgumentException("Grid Size with either rows or columns <=0 is unacceptable!");
		this._rows=rows;
		this._columns=columns;
		this._cells=new ArrayList<Cell>(this._rows*this._columns);
		this._bullets=new ArrayList<Bullet>((this._rows+1)*(this._columns+1));
		this._bestSolution=new ArrayList<Bullet>((this._rows+1)*(this._columns+1));
		Bullet.resetID();
		for(int i=0;i<(this._rows+1)*(this._columns+1);i++){
			this._bullets.add(new Bullet());}
		Cell.resetID();
		for(int i=0;i<(this._rows*this._columns);i++){
			add(new Cell(0));
		}
		for(Bullet b:this._bullets){
			this._bestSolution.add(b.clone());
		}
	}
	
	/**
	 * Generates a random grid with no solution
	 */
	public void randomCells(){
		Random rand=new Random();
		ConsoleInterface tmp=this._ci;
		this._ci=null;
		do{
		for(Cell c:this._cells){
			c.setDigit(rand.nextInt(Cell.MAX_NEIGHBOURS+1));
			c.checkState();
		}
		}while(getRows()>1 && getColumns()>1 && findSolution());
		this._ci=tmp;
		clearBulletStates();
	}
	
	/**
	 * Generates a random grid with a solution
	 */
	public void randomGame(){
		Random rand=new Random();
		for(Bullet b:this._bullets){
			b.setState(rand.nextInt(2)==1);
		}
		for(Cell c:this._cells){
			c.setDigit(c.getEnabledBullets());
		}
		clearBulletStates();
	}
	
	/**
	 * Clears the bullet states (false) of the bullets
	 */
	public void clearBulletStates(){
		for(Bullet b:this._bullets){
			b.setState(false);
		}
	}	
	
	/**
	 * @param c the cell to add to the grid
	 */	
	private void add(Cell c){
		if(this._cells.size()<(this._rows*this._columns)){
			int position=this._cells.size()+(this._cells.size()/this._columns);
			this._bullets.get(position).addNeighbour(c);
			this._bullets.get(position+1).addNeighbour(c);
			this._bullets.get(position+this._columns+1).addNeighbour(c);
			this._bullets.get(position+this._columns+2).addNeighbour(c);
			c.addNeighbour(this._bullets.get(position));
			c.addNeighbour(this._bullets.get(position+1));
			c.addNeighbour(this._bullets.get(position+this._columns+1));
			c.addNeighbour(this._bullets.get(position+this._columns+2));
			this._cells.add(c);
		}
	}
	
	/**
	 * Checks if the current bullet combination is a solution
	 * 
	 * @return true if the solution is right
	 */
	public boolean checkSolution(){
		Iterator<Cell> cellitr=this._cells.iterator();
		boolean accept=true;
		while(accept && cellitr.hasNext()){
			Cell c=cellitr.next();
			if(c.getBulletAmount()==BulletAmount.OVERFLOW || c.getBulletAmount()==BulletAmount.UNDERFLOW){
				accept=false;
			}
		}
		return accept;
	}
	
	private int partitionCells(int left,int right,int pivotIndex){
		Cell pivot=this._cells.get(pivotIndex);
		this._cells.set(pivotIndex,this._cells.get(right));
		this._cells.set(right,pivot);
		for(int i=left;i<right;i++){
			if(this._cells.get(i).getDigit()>pivot.getDigit() || (this._cells.get(i).getDigit()==pivot.getDigit() && this._cells.get(i).hashCode()<pivot.hashCode())){
				Cell tmp=this._cells.get(left);
				this._cells.set(left,this._cells.get(i));
				this._cells.set(i,tmp);
				left++;
			}
		}
		this._cells.set(right,this._cells.get(left));
		this._cells.set(left,pivot);
		return left;
	}
	
	private void quicksortCells(int left,int right){
		if(left<right){
			int pivotIndex=left+(right-left)/2;
			int pivotNewIndex=partitionCells(left,right,pivotIndex);
			quicksortCells(left,pivotNewIndex-1);
			quicksortCells(pivotNewIndex+1,right);
		}
	}
	
	/**
	 * Sorts the Cells using Quicksort according to their digit and their uniqueID
	 */
	public void sortCells(){
		quicksortCells(0,this._cells.size()-1);
	}
	
	/**
	 * A solution search in progress can be aborted with this function.
	 */
	public void abortSolutionFinder(){
		this._abortSolution=true;
	}
	
	/**
	 * Finds all possible bulletplacements for a given Cell that won't violate other cells in the process!
	 * 
	 * @param c the Cell for which combinations should be found
	 * @param position the index of the combination array
	 * @param bulletStates the combination array
	 * @param bcStack the Deque accumulator storing the combination arrays
	 */
	private void recursiveBulletCombinations(Cell c,int position,boolean[] bulletStates,Deque<boolean[]> bcStack){
		if(position>=c.getSize()){
			if(c.getBulletAmount()==BulletAmount.EXACT){
				boolean[] bulletStatesClone=new boolean[c.getSize()];
				System.arraycopy(bulletStates,0,bulletStatesClone,0,c.getSize());
				bcStack.offerFirst(bulletStatesClone);
			}
		}
		else{
			Bullet b=c.getNeighbour(position);
			boolean originalState=b.getState();
			boolean state=true;
			if(c.getBulletAmount()==BulletAmount.UNDERFLOW && !originalState){
				b.setState(state);
				if(b.hasOverflowingNeighbours()){
					state=originalState;
					b.setState(state);
				}
				bulletStates[position]=state;
				recursiveBulletCombinations(c,position+1,bulletStates,bcStack);
				if(state){
					state=false;
					b.setState(state);
					bulletStates[position]=state;
					recursiveBulletCombinations(c,position+1,bulletStates,bcStack);
				}
			}
			else{
				bulletStates[position]=originalState;
				recursiveBulletCombinations(c,position+1,bulletStates,bcStack);
			}
		}
	}
	
	private Deque<boolean[]> getBulletCombinationsGreedy(Cell c){
		Deque<boolean[]> combinations=new ArrayDeque<boolean[]>();
		recursiveBulletCombinations(c.cloneDummy(),0,new boolean[c.getSize()],combinations);
		return combinations;
	}
	
	private Deque<boolean[]> getBulletCombinationsLazy(Cell c){
		Deque<boolean[]> combinations=new ArrayDeque<boolean[]>();
		recursiveBulletCombinations(c,0,new boolean[c.getSize()],combinations);
		return combinations;
	}
	
	private void recursiveBulletCombinationsNoNeighbourCheck(Cell c,int position,boolean[] bulletStates,Deque<boolean[]> bcStack){
		if(position>=c.getSize()){
			if(c.getBulletAmount()==BulletAmount.EXACT){
				boolean[] bulletStatesClone=new boolean[c.getSize()];
				System.arraycopy(bulletStates,0,bulletStatesClone,0,c.getSize());
				bcStack.offerFirst(bulletStatesClone);
			}
		}
		else{
			Bullet b=c.getNeighbour(position);
			boolean originalState=b.getState();
			if(c.getBulletAmount()==BulletAmount.UNDERFLOW && !originalState){
				boolean state=true;
				b.setState(state);
				bulletStates[position]=state;
				recursiveBulletCombinationsNoNeighbourCheck(c,position+1,bulletStates,bcStack);
				state=false;
				b.setState(state);
				bulletStates[position]=state;
				recursiveBulletCombinationsNoNeighbourCheck(c,position+1,bulletStates,bcStack);
			}
			else{
				bulletStates[position]=originalState;
				recursiveBulletCombinationsNoNeighbourCheck(c,position+1,bulletStates,bcStack);
			}
		}
	}
	
	/**
	 * @param c cell to look for combinations
	 * @param position the index of the array
	 * @param bulletStates the array holding the states
	 * @param bcStack the Deque holding the bullet state arrays
	 */
	private void recursiveBulletCombinationsAll(Cell c,int position,boolean[] bulletStates,Deque<boolean[]> bcStack){
		if(position>=c.getSize()){
				boolean[] bulletStatesClone=new boolean[c.getSize()];
				System.arraycopy(bulletStates,0,bulletStatesClone,0,c.getSize());
				bcStack.offerFirst(bulletStatesClone);
		}
		else{
				bulletStates[position]=true;
				recursiveBulletCombinationsAll(c,position+1,bulletStates,bcStack);
				bulletStates[position]=false;
				recursiveBulletCombinationsAll(c,position+1,bulletStates,bcStack);
		}
	}
	
	/**
	 * All possible bullet combinations matching the value in the cell but bot verifying the neighbouring cells
	 * 
	 * @return Deque of bullet states
	 */
	private Deque<boolean[]> getBulletCombinationsNoNeighbourCheckGreedy(Cell c){
		Deque<boolean[]> combinations=new ArrayDeque<boolean[]>();
		recursiveBulletCombinationsNoNeighbourCheck(c.cloneDummy(),0,new boolean[c.getSize()],combinations);
		return combinations;
	}
	
	/**
	 * All possible bullet combinations matching the value of the cell but not verifying the neighbouring cells
	 * 
	 * @return Deque of bullet states
	 */
	private Deque<boolean[]> getBulletCombinationsNoNeighbourCheckLazy(Cell c){
		Deque<boolean[]> combinations=new ArrayDeque<boolean[]>();
		recursiveBulletCombinationsNoNeighbourCheck(c,0,new boolean[c.getSize()],combinations);
		return combinations;
	}
	
	/**
	 * All possible bullet combinations for a cell diregarding its value
	 *
	 * @return a Deque of bullet states
	 */
	private Deque<boolean[]> getBulletCombinationsAll(Cell c){
		Deque<boolean[]> combinations=new ArrayDeque<boolean[]>();
		recursiveBulletCombinationsAll(c,0,new boolean[c.getSize()],combinations);
		return combinations;
	}
	
	/**
	 * Applies a combination of bullets on a Cell
	 * 
	 * @param c the cell that the combination will apply to
	 * @param bulletStates the array containing the combinations
	 */
	private void applyBulletCombination(Cell c,boolean[] bulletStates){
		for(int i=0;i<c.getSize();i++){
			c.getNeighbour(i).setState(bulletStates[i]);
		}
	}
	
	/**
	 * Deactivates all Bullets surrounding a Cell
	 * 
	 * @param c the cell that will have its Bullets reset
	 */
	private void clearBulletCombination(Cell c){
		for(int i=0;i<c.getSize();i++){
			c.getNeighbour(i).setState(false);
		}
	}
	
	/**
	 * Reports how many Cells are violated
	 * 
	 * @return the amount of violated cells (bullets not matching the digit)
	 */
	public int violatedCells(){
		int violations=0;
		for(Cell c:this._cells){
			if(c.getBulletAmount()!=BulletAmount.EXACT) ++violations;
		}
		return violations;
	}
	
	/**
	 * Reports the violations up to the specified cell
	 * 
	 * @param endCell the Cell marking the last Cell to check for violations
	 * @return the amount of violated Cells (bullets not matching the digit)
	 */
	public int violatedCells(Cell endCell){
		int violations=0;
		for(Cell c:this._cells){
			if(c.getBulletAmount()!=BulletAmount.EXACT) ++violations;
			if(c.equals(endCell)) break;
		}
		return violations;
	}
	
	/**
	 * The recursive function that will try to find a solution strictly following the rules.
	 * It will return false if there's no solution!
	 * 
	 * @param stockStack the Stack containing the elements of the grid
	 * @param backtrackStack the Stack containing tested elements
	 * @param combinationsStack the Stack holding the bullet combinations for a cell
	 */
	private boolean recursiveStrictSolution(Deque<Cell> stockStack,Deque<Cell> backtrackStack,Deque<Deque<boolean[]>> combinationsStack){
		if(!stockStack.isEmpty()){
			backtrackStack.offerFirst(stockStack.pollFirst());
			Cell currCell=backtrackStack.peekFirst();
			boolean[] originalStates=new boolean[currCell.getSize()];
			for(int i=0;i<currCell.getSize();i++){
				originalStates[i]=currCell.getNeighbour(i).getState();
			}
			combinationsStack.offerFirst(getBulletCombinationsLazy(currCell));
			while(!combinationsStack.peekFirst().isEmpty()){
				applyBulletCombination(currCell,combinationsStack.peekFirst().pollFirst());
				if(recursiveStrictSolution(stockStack,backtrackStack,combinationsStack)) return true;
			}
			applyBulletCombination(currCell,originalStates);
			if(!this._abortSolution && !backtrackStack.isEmpty()){
				stockStack.offerFirst(backtrackStack.pollFirst());
				combinationsStack.pollFirst();
			}
			return false;
		}
		else{
			saveSolution();
			if(this._ci!=null){
				this._ci.printGridSolution(this);
				this._ci.printf("%n");
			}
			return true;
		}
	}
	
	/**
	 * Should be used first in the search for the best score on a grid without solution
	 * 
	 * @param stockStack the Deque of stock Cells
	 * @param backtrackStack the Deque of backtrack Cells
	 * @param combinationsStack the Deque of bullet state values
	 * @param violations one element array which stores the best score at index 0
	 */
	private boolean recursiveNoSolution(Deque<Cell> stockStack,Deque<Cell> backtrackStack,Deque<Deque<boolean[]>> combinationsStack,int[] violations){
		if(!stockStack.isEmpty()){
			backtrackStack.offerFirst(stockStack.pollFirst());
			Cell currCell=backtrackStack.peekFirst();
			
			boolean[] originalStates=new boolean[currCell.getSize()];
			for(int i=0;i<currCell.getSize();i++){
				originalStates[i]=currCell.getNeighbour(i).getState();
			}
			combinationsStack.offerFirst(getBulletCombinationsNoNeighbourCheckLazy(currCell));
			//combinationsStack.offerFirst(getBulletCombinationsAll(currCell));
			Deque<boolean[]> currPeek=combinationsStack.peekFirst();
			do{
				if(!combinationsStack.peekFirst().isEmpty()){
					applyBulletCombination(currCell,currPeek.pollFirst());
					//if(violatedCells()-(this._cells.size()/((getRows()+getColumns())/2))>violations[0]) continue;
					if(violatedCells(currCell)>violations[0]){ continue;}
				}
				if(recursiveNoSolution(stockStack,backtrackStack,combinationsStack,violations)) return true;
			}while(!currPeek.isEmpty());
			applyBulletCombination(currCell,originalStates);
			if(!this._abortSolution && !backtrackStack.isEmpty()){
				stockStack.offerFirst(backtrackStack.pollFirst());
				combinationsStack.pollFirst();
			}
			return false;
		}
		else{
			int currviolations=violatedCells();
			if(currviolations<violations[0]){
				saveSolution();
				violations[0]=currviolations;
				if(this._ci!=null){
					this._ci.printGridSolution(this);
					this._ci.printf("%n");
				}
			}
			return currviolations==1;
		}
	}
	
	/**
	 * Helper function to store the given grid-bullet-combination in a separate list
	 */
	private void saveSolution(){
		Iterator<Bullet> itrmainbullets=this._bullets.iterator();
		Iterator<Bullet> itrsolutionbullets=this._bestSolution.iterator();
		while(itrmainbullets.hasNext()){
			itrsolutionbullets.next().setState(itrmainbullets.next().getState());
		}
	}
	
	/**
	 * Helper function to load a solution (grid-bullet-combination) from the separate list
	 */
	private void loadSolution(){
		Iterator<Bullet> itrmainbullets=this._bullets.iterator();
		Iterator<Bullet> itrsolutionbullets=this._bestSolution.iterator();
		while(itrsolutionbullets.hasNext()){
			itrmainbullets.next().setState(itrsolutionbullets.next().getState());
		}
	}
	
	/**
	 * Searches for the best solution on a grid without a solution
	 * Should be used after a pass over the grid with {@link #recursiveNoSolution()}
	 * 
	 * @param currCell the currently treated cell, the recursion should be started with the first cell in the grid
	 * @param stockCellStack the Stack of stock Cells
	 * @param stockStack the Stack of stock Bullets
	 * @param backtrackStack the temporary Stack for backtracking friendliness
	 * @param combinationsStack the Stack of the different bullet combination
	 * @param violations the int value of the violations stored at index 0 (array used for it to be accessible through recursions)
	 */
	private boolean recursiveMagicSolution(Deque<Cell> stockCellStack,Deque<Bullet> stockStack,Deque<Bullet> backtrackStack,Deque<Deque<Boolean>> combinationsStack,int[] violations){
		if(!stockStack.isEmpty()){
			backtrackStack.offerFirst(stockStack.pollFirst());
			combinationsStack.offerFirst(new ArrayDeque<Boolean>());
			Bullet currBullet=backtrackStack.peekFirst();
			boolean originalState=currBullet.getState();
			combinationsStack.peekFirst().offerFirst(new Boolean(false));
			//if(currBullet.getState()){
				combinationsStack.peekFirst().offerFirst(new Boolean(true));
			//}
			Deque<Boolean> currPeek=combinationsStack.peekFirst();
			while(!this._abortSolution && !currPeek.isEmpty()){
				boolean currValue=currPeek.pollFirst().booleanValue();
				if(currBullet.getState()!=currValue) currBullet.setState(currValue);
				//if(violatedCells()-(this._cells.size()/((getRows()+getColumns())/2))>violations[0]){ continue;}
				if(violatedCells(currBullet.getNeighbour(0))>violations[0]){ continue;}
				if(recursiveMagicSolution(stockCellStack,stockStack,backtrackStack,combinationsStack,violations)) return true;
			}
			currBullet.setState(originalState);
			if(!backtrackStack.isEmpty()){
				stockStack.offerFirst(backtrackStack.pollFirst());
				combinationsStack.pollFirst();
			}
			return false;
		}
		else{
			int currViolations=violatedCells();
			if(currViolations<violations[0]){
				saveSolution();
				violations[0]=currViolations;
				if(this._ci!=null){
					this._ci.printGridSolution(this);
					this._ci.printf("%n");
				}
				if(currViolations==1){
				return true;
				}
			}
			return false;
		}
	}		
	
	/**
	 * Try to find a solution with no violations
	 * 
	 * @return true if the grid has a solution (violations==0)
	 */
	public boolean findSolution(){
		int[] violations={violatedCells()};
		clearBulletStates();
		this._abortSolution=false;
		long startTime=System.currentTimeMillis();
		boolean truth=recursiveStrictSolution(new ArrayDeque<Cell>(this._cells),new ArrayDeque<Cell>(),new ArrayDeque<Deque<boolean[]>>());
		long endTime=System.currentTimeMillis();
		this._abortSolution=false;
		if(this._ci!=null && truth) this._ci.printf("Elapsed Time: %dms%n",(endTime-startTime));
		loadSolution();
		return truth;
	}
	
	/**
	 * This solution search algorithm trys to find the best possible bullet states
	 * to bring the amount of violations to its lowest
	 * 
	 * @return true if it managed to find an amount of violations ==1
	 */
	public boolean findNoSolution(){
		int[] violations={violatedCells()};
		clearBulletStates();
		this._abortSolution=false;
		long startTime=System.currentTimeMillis();	
		//boolean truth=recursiveMagicSolution(this._cells.get(0),new ArrayDeque<Cell>(this._cells),new ArrayDeque<Bullet>(this._bullets),new ArrayDeque<Bullet>(),new ArrayDeque<Deque<Boolean>>(),violations);
		boolean truth=recursiveNoSolution(new ArrayDeque<Cell>(this._cells),new ArrayDeque<Cell>(),new ArrayDeque<Deque<boolean[]>>(),violations);
		loadSolution();
		//boolean truth=false;
		if(recursiveMagicSolution(new ArrayDeque<Cell>(this._cells),new ArrayDeque<Bullet>(this._bullets),new ArrayDeque<Bullet>(),new ArrayDeque<Deque<Boolean>>(),violations)) truth=true;
		long endTime=System.currentTimeMillis();
		this._abortSolution=false;
		if(this._ci!=null) this._ci.printf("Elapsed Time: %dms%n",(endTime-startTime));
		loadSolution();
		return truth;
	}
		
	
	/**
	 * Return the iterator over the bullets
	 * 
	 * @return an Iterator over the Bullets
	 */
	public Iterator<Bullet> getBulletIterator(){
		return this._bullets.iterator();
	}
	
	/**
	 * Return the iterator over the cells
	 * 
	 * @return an iterator over the Cells
	 */
	public Iterator<Cell> getCellIterator(){
		return this._cells.iterator();
	}
	
	/**
	 * Return the solution bullet iterator
	 * 
	 * @return the iterator over the solution bullets
	 */
	public Iterator<Bullet> getSolutionIterator(){
		return this._bestSolution.iterator();
	}
	
	/**
	 * Amount of rows
	 * 
	 * @return the amount of rows
	 */
	public int getRows(){
		return this._rows;
	}
	
	/**
	 * Amount of columns
	 * 
	 * @return amount of columns
	 */
	public int getColumns(){
		return this._columns;
	}
	
	/**
	 * Intended to be used by a Console interface
	 * @param row the row of the bullet commencing at 1
	 * @param column the column of the bullet commencing at 1
	 * @return false if indexes are out of bounds
	 */
	public boolean toggleBullet(int row,int column){
		if(row>getRows()+1 || column>getColumns()+1 || row<1 || column<1) return false;
		else{
			Bullet b=this._bullets.get(((row-1)*(getColumns()+1))+(column-1));
			b.setState(!b.getState());
			return true;
		}
	}
}
